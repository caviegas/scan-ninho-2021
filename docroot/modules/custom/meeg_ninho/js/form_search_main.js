(function ($, jQuery) {
    /**
     * Preventing the search form input from submiting on ENTER keypress.
     */
    $('#views-exposed-form-search-main-search-main').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            return false;
        }
    })
})(jQuery);