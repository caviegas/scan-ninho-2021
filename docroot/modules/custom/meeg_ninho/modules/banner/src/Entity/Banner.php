<?php

namespace Drupal\meeg_ninho_banner\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\meeg_ninho_banner\Entity\BannerInterface;

/**
 * Defines the banner entity class.
 * 
 * @ContentEntityType(
 *  id = "meeg_ninho_banner",
 *  label = @Translation("Entidade Banner"),
 *  label_collection = @Translation("Banner"),
 *  label_singular = @Translation("Banner"),
 *  label_plural = @Translation("Banners"),
 *  label_count = @PluralTranslation(
 *      singular = "@count banner",
 *      plural = "@count banners",
 *  ),
 *  handlers = {
 *      "access" = "Drupal\meeg_ninho_banner\Controller\BannerAccessControlHandler",
 *      "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *      "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *      "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *      "list_builder" = "Drupal\meeg_ninho_banner\Controller\BannerListBuilder",
 *      "views_data" = "Drupal\meeg_ninho_banner\Controller\BannerViewsData",
 *      "form" = {
 *          "add" = "Drupal\meeg_ninho_banner\Form\BannerForm",
 *          "edit" = "Drupal\meeg_ninho_banner\Form\BannerForm",
 *          "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *      },
 *  },
 *  admin_permission = "administer meeg_ninho_banner entity",
 *  fieldable = TRUE,
 *  base_table = "meeg_ninho_banner",
 *  entity_keys = {
 *      "id" = "id",
 *      "label" = "title",
 *      "created" = "created",
 *      "changed" = "changed",
 *      "status" = "status",
 *      "published" = "status",
 *  },
 *  links = {
 *      "canonical" = "/banner/{meeg_ninho_banner}",
 *      "add" = "/banner/cadastrar/",
 *      "edit" = "/banner/editar/{meeg_ninho_banner}",
 *      "delete" = "/banner/deletar/{meeg_ninho_banner}",
 *      "collection" = "/banner",
 *  },
 *  field_ui_base_rote = "entity.meeg_ninho_banner.settings",
 *  common_reference_target = TRUE,
 * )
 */
class Banner extends ContentEntityBase implements BannerInterface
{
    use EntityChangedTrait;
    use EntityPublishedTrait;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->get('title')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->set('title', $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLink()
    {
        return $this->get('link')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setLink($link)
    {
        $this->set('link', $link);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * Get the image entity.
     */
    public function getImage()
    {
        return $this->get('image')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);
        $fields += static::publishedBaseFieldDefinitions($entity_type);

        $fields['title'] = BaseFieldDefinition::create('string')
            ->setLabel('Título')
            ->setDescription('O título do banner.')
            ->setRequired(TRUE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => 1,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['link'] = BaseFieldDefinition::create('string')
            ->setLabel('Link')
            ->setDescription('A URL para a qual o banner redirecionará.')
            ->setRequired(TRUE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => 2,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 2,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['image'] = BaseFieldDefinition::create('image')
            ->setLabel('Imagem')
            ->setDescription('A imagem do banner.')
            ->setRequired(TRUE)
            ->setRevisionable(TRUE)
            ->setDisplayOptions('view', [
                'type' => 'image',
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'large',
                ],
                'weight' => 4,
            ])
            ->setDisplayOptions('form',  [
                'type' => 'image',
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'thumbnail',
                ],
                'weight' => 4,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE)
            ->setReadOnly(TRUE);

        $fields['status']
            ->setLabel('Ativado')
            ->setDescription('Define se o banner está ativado ou não para mostrar ao público.')
            ->setDisplayOptions('form', [
                'type' => 'boolean_checkbox',
                'settings' => [
                    'display_label' => TRUE,
                ],
                'weight' => 7,
            ])
            ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel('Criado')
            ->setDescription('A data e hora de quando o banner foi criado.')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
      
        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel('Alterado')
            ->setDescription('A data e hora mais recente de quando o banner foi editado.');

        return $fields;
    }
}