<?php

namespace Drupal\meeg_ninho_banner\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface defining a FAQ entity.
 */
interface BannerInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface
{
    /**
     * Gets the banner title.
     * 
     * @return string
     */
    public function getTitle();

    /**
     * Sets the banner title.
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title);

    /**
     * Gets the banner link URL.
     * 
     * @return string
     */
    public function getLink();

    /**
     * Sets the banner link URL.
     *
     * @param string $link
     *
     * @return $this
     */
    public function setLink($link);

    /**
     * Gets the banner creation timestamp.
     *
     * @return int
     */
    public function getCreatedTime();

    /**
     * Sets the banner creation timestamp.
     *
     * @param int $timestamp
     *
     * @return $this
     */
    public function setCreatedTime($timestamp);
}