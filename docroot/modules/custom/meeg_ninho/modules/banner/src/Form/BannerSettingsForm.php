<?php

namespace Drupal\meeg_ninho_banner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BannerSettingsForm.
 * 
 * @package Drupal\meeg_ninho_banner\Form
 */
class BannerSettingsForm extends FormBase
{
    /**
     * Returns a unique string identifying the form.
     * 
     * @return string
     *  The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'meeg_ninho_banner_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Empty implementation of the abstract submit class.
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['banner_settings']['#markup'] = 'Configuração para o Banner. Configure os campos aqui.';
        return $form;
    }
}