<?php

namespace Drupal\meeg_ninho_banner\Controller;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;

/**
 * Access controller for the banner entity.
 *
 * @see \Drupal\meeg_ninho_banner\Entity\Banner.
 */
class BannerAccessControlHandler extends EntityAccessControlHandler
{
    /**
     * {@inheritdoc}
     * 
     * Link the activities to the permissions. checkAccess is called with the
     * $operation as defined in the routing.yml file.
     */
    protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
    {
        switch ($operation) {
            case 'view':
                return AccessResult::allowedIfHasPermission($account, 'view meeg_ninho_banner entity');

            case 'edit':
                return AccessResult::allowedIfHasPermission($account, 'edit meeg_ninho_banner entity');

            case 'delete':
                return AccessResult::allowedIfHasPermission($account, 'delete meeg_ninho_banner entity');
        }

        return AccessResult::allowed();
    }

    /**
     * {@inheritdoc}
     * 
     * Separate from the checkAccess because the entity does not yet exist, it
     * will be created during the 'add' process.
     */
    protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL)
    {
        return AccessResult::allowedIfHasPermission($account, 'add meeg_ninho_banner entity');
    }
}