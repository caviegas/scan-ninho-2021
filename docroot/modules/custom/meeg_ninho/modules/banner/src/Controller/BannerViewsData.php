<?php

namespace Drupal\meeg_ninho_banner\Controller;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the banner entity.
 */
class BannerViewsData extends EntityViewsData
{
    /**
     * {@inheritdoc}
     */
    public function getViewsData()
    {
        $data = parent::getViewsData();
        return $data;
    }
}