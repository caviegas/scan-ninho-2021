<?php

namespace Drupal\meeg_ninho_banner\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\UrlGeneratorInterface;

/**
 * Defines the list builder for banners.
 */
class BannerListBuilder extends EntityListBuilder
{
    /**
     * The url generator.
     *
     * @var \Drupal\Core\Routing\UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
    * {@inheritdoc}
    */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
    {
        return new static(
            $entity_type,
            $container->get('entity_type.manager')->getStorage($entity_type->id()),
            $container->get('url_generator')
        );
    }

    /**
    * Constructs a new BannerListBuilder object.
    *
    * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
    *
    * @param \Drupal\Core\Entity\EntityStorageInterface $storage
    *
    * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
    */
    public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator)
    {
        parent::__construct($entity_type, $storage);
        $this->urlGenerator = $url_generator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildHeader()
    {
        $header['title'] = 'Título';
        $header['link'] = 'Link';
        $header['status'] = 'Ativado';

        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity)
    {
        $row['title']['data'] = [
            '#type' => 'link',
            '#title' => $entity->label(),
        ] + $entity->toUrl()->toRenderArray();
        $row['link']['data'] = [
            '#type' => 'link',
            '#title' => $entity->getLink(),
        ] + Url::fromUri($entity->getLink())->toRenderArray();
        $row['status'] = $entity->isPublished() ? 'Sim' : 'Não';

        return $row + parent::buildRow($entity);
    }

    /**
     * {@inheritdoc}
     */
    // public function render()
    // {
    //     $build['description'] = [
    //         '#markup' => $this->t('Esta entidade implementa um modelo de Produtos. Esses produtos podem possuir campos e serem configurados na <a href="@adminlink">página de admin de Produtos</a>.',[
    //             '@adminlink' => $this->urlGenerator->generateFromRoute('entity.meeg_ninho_product.product_settings'),
    //         ]),
    //     ];
        
    //     $build += parent::render();
    //     return $build;
    // }
}