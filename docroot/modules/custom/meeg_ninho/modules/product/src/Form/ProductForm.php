<?php

namespace Drupal\meeg_ninho_product\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the product add/edit form.
 * 
 * @ingroup meeg_ninho_product
 */
class ProductForm extends ContentEntityForm
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form =  parent::buildForm($form, $form_state);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        $form_state->setRedirect('entity.meeg_ninho_product.collection');
        $entity = $this->getEntity();
        $entity->save();
    }
}