<?php

namespace Drupal\meeg_ninho_product\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a product entity.
 *
 * @ingroup meeg_ninho_product
 */
class ProductDeleteForm extends ContentEntityConfirmFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return $this->t('Tem certeza que quer deletar %title?', [
            '%title' => $this->entity->getTitle(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl()
    {
        return new Url('entity.meeg_ninho_product.collection');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText()
    {
        return $this->t('Deletar');
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $entity = $this->getEntity();
        $entity->delete();

        $this->logger('meeg_ninho_product')->notice('%title foi deletado.', [
            '%title' => $this->entity->getTitle(),
        ]);

        $form_state->setRedirect('entity.meeg_ninho_product.collection');
    }
}