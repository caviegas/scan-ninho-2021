<?php

namespace Drupal\meeg_ninho_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductSettingsForm.
 * 
 * @package Drupal\meeg_ninho_product\Form
 * 
 * @ingroup meeg_ninho_product
 */
class ProductSettingsForm extends FormBase
{
    /**
     * Returns a unique string identifying the form.
     * 
     * @return string
     *  The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'meeg_ninho_product_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Empty implementation of the abstract submit class.
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['product_settings']['#markup'] = 'Configuração para Produto. Configure os campos aqui.';
        return $form;
    }
}