<?php

namespace Drupal\meeg_ninho_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Defines the interface for products.
 */
interface ProductInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface
{
    /**
     * Gets the product title.
     * 
     * @return string
     */
    public function getTitle();

    /**
     * Sets the product title.
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title);

    /**
     * Gets the product description.
     * 
     * @return string
     */
    public function getDescription();

    /**
     * Sets the product description.
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * Gets the class taxonomy term entity.
     * 
     * @return Drupal\taxonomy\Entity\Term
     */
    public function getClass();

    /**
     * Sets the class taxonomy term entity.
     * 
     * @param Drupal\taxonomy\Entity\Term $class
     * 
     * @return $this
     */
    public function setClass(TermInterface $class);

    /**
     * Gets the product components.
     * 
     * @return string
     */
    public function getComponents();

    /**
     * Sets the product components.
     *
     * @param string $components
     *
     * @return $this
     */
    public function setComponents($components);

    /**
     * Gets the product preservation.
     * 
     * @return string
     */
    public function getPreservation();

    /**
     * Sets the product preservation.
     *
     * @param string $preservation
     *
     * @return $this
     */
    public function setPreservation($preservation);

    /**
     * Gets the product nutritional information.
     * 
     * @return string
     */
    public function getNutritional();

    /**
     * Sets the product nutritional information.
     *
     * @param string $nutritional
     *
     * @return $this
     */
    public function setNutritional($nutritional);

    /**
     * Gets the product references.
     * 
     * @return string
     */
    public function getReferences();

    /**
     * Sets the product references.
     *
     * @param string $references
     *
     * @return $this
     */
    public function setReferences($references);

    /**
     * Gets the product creation timestamp.
     *
     * @return int
     *  The product creation timestamp.
     */
    public function getCreatedTime();

    /**
     * Sets the product creation timestamp.
     *
     * @param int $timestamp
     *  The product creation timestamp.
     *
     * @return $this
     */
    public function setCreatedTime($timestamp);

    /**
     * Gets the product lett ID.
     *
     * @return string
     */
    public function getLettId();

    /**
     * Sets the product lett ID.
     *
     * @param string $lett_id
     *
     * @return $this
     */
    public function setLettId($lett_id);
}