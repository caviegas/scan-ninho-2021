<?php

namespace Drupal\meeg_ninho_product\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\meeg_ninho_product\Entity\ProductInterface;

/**
 * Defines the product entity class.
 * 
 * @ingroup product
 * 
 * @ContentEntityType(
 *  id = "meeg_ninho_product",
 *  label = @Translation("Entidade Produto"),
 *  label_collection = @Translation("Produtos"),
 *  label_singular = @Translation("Produto"),
 *  label_plural = @Translation("Produtos"),
 *  label_count = @PluralTranslation(
 *      singular = "@count produto",
 *      plural = "@count produtos",
 *  ),
 *  handlers = {
 *      "access" = "Drupal\meeg_ninho_product\Controller\ProductAccessControlHandler",
 *      "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *      "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *      "view_builder" = "Drupal\meeg_ninho_product\Controller\ProductViewBuilder",
 *      "list_builder" = "Drupal\meeg_ninho_product\Controller\ProductListBuilder",
 *      "views_data" = "Drupal\meeg_ninho_product\Controller\ProductViewsData",
 *      "form" = {
 *          "add" = "Drupal\meeg_ninho_product\Form\ProductForm",
 *          "edit" = "Drupal\meeg_ninho_product\Form\ProductForm",
 *          "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *      },
 *  },
 *  admin_permission = "administer meeg_ninho_product entity",
 *  fieldable = TRUE,
 *  base_table = "meeg_ninho_product",
 *  entity_keys = {
 *      "id" = "id",
 *      "label" = "title",
 *      "tid" = "class_id",
 *      "created" = "created",
 *      "changed" = "changed",
 *      "status" = "status",
 *      "published" = "status",
 *  },
 *  links = {
 *      "canonical" = "/produto/{meeg_ninho_product}",
 *      "add-form" = "/produto/cadastrar/",
 *      "edit-form" = "/produto/editar/{meeg_ninho_product}",
 *      "delete-form" = "/produto/deletar/{meeg_ninho_product}",
 *      "collection" = "/produtos",
 *  },
 *  field_ui_base_rote = "entity.meeg_ninho_product.product_settings",
 *  common_reference_target = TRUE,
 * )
 */
class Product extends ContentEntityBase implements ProductInterface
{
    use EntityChangedTrait;
    use EntityPublishedTrait;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->get('title')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->set('title', $title);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->get('description')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->set('description', $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->get('class_id')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function setClass(TermInterface $class)
    {
        $this->set('class_id', $class->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getComponents()
    {
        return $this->get('components')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setComponents($components)
    {
        $this->set('components', $components);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPreservation()
    {
        return $this->get('preservation')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setPreservation($preservation)
    {
        $this->set('preservation', $preservation);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getNutritional()
    {
        return $this->get('nutritional')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setNutritional($nutritional)
    {
        $this->set('nutritional', $nutritional);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getReferences()
    {
        return $this->get('references')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setReferences($references)
    {
        $this->set('references', $references);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    public function getImage()
    {
        return $this->get('image')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getLettId()
    {
        return $this->get('lett_id')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setLettId($lett_id)
    {
        $this->set('lett_id', $lett_id);
        return $this;
    }

    /**
     * Get the array of package paragraphs.
     * 
     * @param boolean $published
     *  Set FALSE to get all content.
     * 
     * @return array
     */
    public function getPackages($published = true)
    {
        $data = [];
        foreach ($this->get('paragraph_package') as $package) {
            $data[$package->entity->id()] = $package->entity;
            if ($published === true && $package->entity->isPublished() === false) {
                unset($data[$package->entity->id()]);
            }
        }
        return $data;
    }

    /**
     * Get the slug of the product if it has questions from FAQ.
     * 
     * @return string|null
     *  Returns the path alias if true, null if false.
     */
    public function getSlug()
    {
        $faqHelper = \Drupal::getContainer()->get('meeg_ninho.faq_helper');

        if ($faqHelper->hasQuestions($this->id(), 'product_id') === true) {
            return $this->toUrl()->toString();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);
        $fields += static::publishedBaseFieldDefinitions($entity_type);

        $fields['title'] = BaseFieldDefinition::create('string')
            ->setLabel('Nome')
            ->setDescription('O nome do produto.')
            ->setRequired(TRUE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => 1,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['description'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Descrição')
            ->setDescription('A descrição do produto.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'text_textarea_with_summary',
                'weight' => 2,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 3,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['class_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel('Categoria')
            ->setDescription('A categoria do produto.')
            ->setRequired(TRUE)
            ->setSettings([
                'target_type' => 'taxonomy_term',
                'handler' => 'default:taxonomy_term',
                'handler_settings' => [
                    'target_bundles' => [
                        'class' => 'class',
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 3,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
                'weight' => 2,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['lett_id'] = BaseFieldDefinition::create('string')
            ->setLabel('Lett ID')
            ->setDescription('O lett ID do produto.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'max_length' => 255,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => 12,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['components'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Ingredientes')
            ->setDescription('Os ingredientes do produto.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'text_textarea_with_summary',
                'weight' => 5,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 5,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['preservation'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Conservação')
            ->setDescription('As informações de conservação do produto.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'text_textarea_with_summary',
                'weight' => 6,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 6,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['nutritional'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Informações Nutricionais')
            ->setDescription('As informações nutricionais do produto.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'text_textarea_with_summary',
                'weight' => 7,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 7,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        /** PARAGRAPHS */
        $fields['paragraph_package'] = BaseFieldDefinition::create('entity_reference_revisions')
            ->setLabel('Embalagens')
            ->setDescription('Conjunto de embalagens do produto.')
            ->setRequired(FALSE)
            ->setCardinality(-1)
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'paragraph')
            ->setSetting('handler', 'default:paragraph')
            ->setSetting('handler_settings', [
                'target_bundles' => [
                    'product_package' => 'product_package'
                ],
            ])
            ->setTranslatable(FALSE)
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_paragraphs',
                'weight' => 8,
                'settings' => [
                    'title' => 'Embalagem',
                    'title_plural' => 'Embalagens',
                    'edit_mode' => 'closed_expand_nested',
                    'closed_mode' => 'summary',
                    'autocollapse' => 'none',
                    'add_mode' => 'modal',
                    'form_display_mode' => 'default',
                    'default_paragraph_type' => 'product_package',
                    'features' => [
                        'add_above' => 'add_above',
                        'collapse_edit_all' => 'collapse_edit_all',
                        'duplicate' => 'duplicate',
                    ]
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        /** PARAGRAPHS */
        $fields['paragraph_howtoprepare'] = BaseFieldDefinition::create('entity_reference_revisions')
            ->setLabel('Modo de preparo')
            ->setDescription('Conteúdo sobre o "modo de preparo".')
            ->setRequired(FALSE)
            ->setCardinality(-1)
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'paragraph')
            ->setSetting('handler', 'default:paragraph')
            ->setSetting('handler_settings', [
                'target_bundles' => [
                    'howtoprepare_block' => 'howtoprepare_block'
                ],
            ])
            ->setTranslatable(FALSE)
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_paragraphs',
                'weight' => 9,
                'settings' => [
                    'title' => 'Bloco',
                    'title_plural' => 'Blocos',
                    'edit_mode' => 'closed_expand_nested',
                    'closed_mode' => 'summary',
                    'autocollapse' => 'none',
                    'add_mode' => 'modal',
                    'form_display_mode' => 'default',
                    'default_paragraph_type' => 'howtoprepare_block',
                    'features' => [
                        'add_above' => 'add_above',
                        'collapse_edit_all' => 'collapse_edit_all',
                        'duplicate' => 'duplicate',
                    ]
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['obs_howtoprepare'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Modo de preparo: Observação')
            ->setDescription('Texto de observação para o "modo de preparo".')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 4,
                ),
                'weight' => 10,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['references'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Referências')
            ->setDescription('Referências para artigos e similares.')
            ->setRequired(FALSE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => array(
                    'rows' => 5,
                ),
                'weight' => 11,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['path'] = BaseFieldDefinition::create('path')
            ->setLabel('URL amigável')
            ->setDescription('A URL amigável do produto.')
            ->setDisplayOptions('form', [
                'type' => 'path',
                'weight' => 13,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setComputed(TRUE);

        $fields['status']
            ->setLabel('Ativado')
            ->setDescription('Define se o produto está ativado ou não para mostrar ao público.')
            ->setDisplayOptions('form', [
                'type' => 'boolean_checkbox',
                'settings' => [
                    'display_label' => TRUE,
                ],
                'weight' => 14,
            ])
            ->setDisplayConfigurable('form', TRUE);

        $fields['image'] = BaseFieldDefinition::create('image')
            ->setLabel('Imagem')
            ->setDescription('A imagem do produto.')
            ->setRequired(FALSE)
            ->setRevisionable(TRUE)
            ->setDisplayOptions('view', [
                'type' => 'image',
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'large',
                ],
                'weight' => 4,
            ])
            ->setDisplayOptions('form',  [
                'type' => 'image',
                'label' => 'hidden',
                'settings' => [
                    'image_style' => 'thumbnail',
                ],
                'weight' => 4,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE)
            ->setReadOnly(TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel('Criado')
            ->setDescription('A data e hora de quando o produto foi criado.')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
      
        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel('Alterado')
            ->setDescription('A data e hora mais recente de quando o produto foi editado.');

        return $fields;
    }
}