<?php

namespace Drupal\meeg_ninho_product\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\layout_builder\Entity\LayoutEntityDisplayInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;

/**
 * Defines the entity view builder for products.
 */
class ProductViewBuilder extends EntityViewBuilder
{
    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * The faq entity storage.
     *
     * @var \Drupal\meeg_ninho_faq\Entity\FAQ
     */
    protected $faq;

    /**
     * The product entity storage.
     *
     * @var \Drupal\meeg_ninho_product\Entity\Product
     */
    protected $product;

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
    {
        return new static(
            $entity_type,
            $container->get('entity.repository'),
            $container->get('language_manager'),
            $container->get('theme.registry'),
            $container->get('entity_display.repository'),
            $container->get('entity_type.manager'),
        );
    }

    /**
     * Constructs a new ProductViewBuilder object.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     * 
     * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
     * 
     * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
     * 
     * @param \Drupal\Core\Theme\Registry $theme_registry
     * 
     * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
     * 
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     */
    public function __construct(EntityTypeInterface $entity_type, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeManagerInterface $entity_type_manager)
    {
        parent::__construct(
            $entity_type,
            $entity_repository,
            $language_manager,
            $theme_registry,
            $entity_display_repository
        );

        $this->entityTypeManager = $entity_type_manager;
        $this->faq = $entity_type_manager->getStorage('meeg_ninho_faq');
        $this->product = $entity_type_manager->getStorage('meeg_ninho_product');
    }

    /**
     * {@inheritdoc}
     */
    public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL)
    {
        $build = parent::view($entity, $view_mode, $langcode);

        $faq_product = $this->faq->loadByProperties([
            'status' => true,
            'product_id' => $entity->id(),
        ]);
        $build['#faq_product'] = $faq_product;

        $faq_class = $this->faq->loadByProperties([
            'status' => true,
            'class_id' => $entity->getClass()->id(),
        ]);
        $build['#faq_class'] = $faq_class;

        $other_products = $this->product->loadMultiple(
            $this->product->getQuery()
                ->condition('status', true)
                ->condition('class_id', $entity->class_id->target_id)
                ->condition('id', $entity->id(), '<>')
                ->execute()
        );
        $build['#other_products'] = $other_products;

        return $build;
    }
}