<?php

namespace Drupal\meeg_ninho_product\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\Request;

class ProductPagesController extends ControllerBase
{
    /**
     * Entity Product.
     * 
     * @var Drupal\meeg_ninho_product\Entity\Product
     */
    protected $product;

    /**
     * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
     */
    public function __construct(EntityTypeManager $entity_type_manager)
    {
        $this->product = $entity_type_manager->getStorage('meeg_ninho_product');
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager'),
        );
    }

    /**
     * 
     */
    public function whereToFind(Request $request)
    {
        $gtin = $request->query->get('gtin');

        if (!empty($gtin)) {
            $product = $this->product->loadByProperties([
                'status' => true,
                'lett_id' => $gtin,
            ]);
        }

        $data = [];

        if (!empty($product)) {
            $data['product'] = reset($product);
        }

        return [
            '#theme' => 'where-to-find',
            '#data' => $data,
        ];
    }
}