<?php

namespace Drupal\meeg_ninho_faq\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a FAQ entity.
 *
 * @ingroup meeg_ninho
 */
class FAQDeleteForm extends ContentEntityConfirmFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return $this->t('Tem certeza que quer deletar a pergunta? "%question"', [
            '%question' => $this->entity->getQuestion(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl()
    {
        return new Url('entity.meeg_ninho_faq.collection');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText()
    {
        return $this->t('Deletar');
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $entity = $this->getEntity();
        $entity->delete();

        $this->logger('meeg_ninho_faq')->notice('A pergunta foi deletada. "%question"', [
            '%question' => $this->entity->getQuestion(),
        ]);

        $form_state->setRedirect('entity.meeg_ninho_faq.collection');
    }
}