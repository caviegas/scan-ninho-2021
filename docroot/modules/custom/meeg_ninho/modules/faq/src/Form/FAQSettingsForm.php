<?php

namespace Drupal\meeg_ninho_faq\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FAQSettingsForm.
 * 
 * @package Drupal\meeg_ninho_faq\Form
 * 
 * @ingroup meeg_ninho
 */
class FAQSettingsForm extends FormBase
{
    /**
     * Returns a unique string identifying the form.
     * 
     * @return string
     *  The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'meeg_ninho_faq_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Empty implementation of the abstract submit class.
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['faq_settings']['#markup'] = 'Configuração para o FAQ. Configure os campos aqui.';
        return $form;
    }
}