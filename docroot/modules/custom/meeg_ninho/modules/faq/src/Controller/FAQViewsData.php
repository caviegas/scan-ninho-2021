<?php

namespace Drupal\meeg_ninho_faq\Controller;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the FAQ entity.
 */
class FAQViewsData extends EntityViewsData
{
    /**
     * {@inheritdoc}
     */
    public function getViewsData()
    {
        $data = parent::getViewsData();
        return $data;
    }
}