<?php

namespace Drupal\meeg_ninho_faq\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\meeg_ninho_faq\FAQHelper;

class FAQController extends ControllerBase
{
    /**
     * Entity FAQ.
     * 
     * @var Drupal\meeg_ninho_faq\Entity\FAQ
     */
    protected $faq;

    /**
     * FAQ Helper.
     * 
     * @var Drupal\meeg_ninho_faq\FAQHelper
     */
    protected $helper;

    /**
     * Entity Term.
     * 
     * @var Drupal\taxonomy\Entity\Term
     */
    protected $term;

    /**
     * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
     */
    public function __construct(EntityTypeManager $entity_type_manager)
    {
        $this->faq = $entity_type_manager->getStorage('meeg_ninho_faq');
        $this->term = $entity_type_manager->getStorage('taxonomy_term');
        $this->helper = \Drupal::getContainer()->get('meeg_ninho.faq_helper');
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager'),
        );
    }

    public function list()
    {
        $path = $this->helper->checkFAQPath();
        $selector = $this->helper->getFAQSelector($path);
        $faq = $this->helper->getFAQSortedByTag($path);

        $data = [
            'selector' => $selector,
            'faq' => $faq,
        ];

        return [
            '#theme' => 'faq',
            '#data' => $data,
        ];
    }
 }