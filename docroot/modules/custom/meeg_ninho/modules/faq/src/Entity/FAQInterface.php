<?php

namespace Drupal\meeg_ninho_faq\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface defining a FAQ entity.
 */
interface FAQInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface
{
    /**
     * Gets the FAQ question.
     * 
     * @return string
     */
    public function getQuestion();

    /**
     * Sets the FAQ question.
     *
     * @param string $question
     *
     * @return $this
     */
    public function setQuestion($question);

    /**
     * Gets the FAQ answer.
     * 
     * @return string
     */
    public function getAnswer();

    /**
     * Sets the FAQ answer.
     *
     * @param string $answer
     *
     * @return $this
     */
    public function setAnswer($answer);

    /**
     * Gets the product entity.
     * 
     * @return Drupal\meeg_ninho_product\Entity\Product
     */
    public function getProduct();

    /**
     * Sets the product entity.
     * 
     * @param Drupal\meeg_ninho_product\Entity\Product $product
     * 
     * @return $this
     */
    public function setProduct(ProductInterface $product);

    /**
     * Gets the theme taxonomy term entity.
     * 
     * @return Drupal\taxonomy\Entity\Term
     */
    public function getTheme();

    /**
     * Sets the theme taxonomy term entity.
     * 
     * @param Drupal\taxonomy\Entity\Term $theme
     * 
     * @return $this
     */
    public function setTheme(TermInterface $theme);

    /**
     * Gets the class taxonomy term entity.
     * 
     * @return Drupal\taxonomy\Entity\Term
     */
    public function getClass();

    /**
     * Sets the class taxonomy term entity.
     * 
     * @param Drupal\taxonomy\Entity\Term $class
     * 
     * @return $this
     */
    public function setClass(TermInterface $class);

    /**
     * Gets the FAQ creation timestamp.
     *
     * @return int
     */
    public function getCreatedTime();

    /**
     * Sets the FAQ creation timestamp.
     *
     * @param int $timestamp
     *
     * @return $this
     */
    public function setCreatedTime($timestamp);
}