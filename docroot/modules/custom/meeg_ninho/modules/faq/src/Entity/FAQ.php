<?php

namespace Drupal\meeg_ninho_faq\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\meeg_ninho_faq\Entity\FAQInterface;

/**
 * Defines the FAQ entity class.
 * 
 * @ingroup faq
 * 
 * @ContentEntityType(
 *  id = "meeg_ninho_faq",
 *  label = @Translation("Entidade FAQ"),
 *  label_collection = @Translation("FAQ"),
 *  label_singular = @Translation("FAQ"),
 *  label_plural = @Translation("FAQ"),
 *  label_count = @PluralTranslation(
 *      singular = "@count FAQ",
 *      plural = "@count FAQ",
 *  ),
 *  handlers = {
 *      "access" = "Drupal\meeg_ninho_faq\Controller\FAQAccessControlHandler",
 *      "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *      "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *      "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *      "list_builder" = "Drupal\meeg_ninho_faq\Controller\FAQListBuilder",
 *      "views_data" = "Drupal\meeg_ninho_faq\Controller\FAQViewsData",
 *      "form" = {
 *          "add" = "Drupal\meeg_ninho_faq\Form\FAQForm",
 *          "edit" = "Drupal\meeg_ninho_faq\Form\FAQForm",
 *          "delete" = "Drupal\meeg_ninho_faq\Form\FAQDeleteForm",
 *      },
 *  },
 *  admin_permission = "administer meeg_ninho_faq entity",
 *  fieldable = TRUE,
 *  base_table = "meeg_ninho_faq",
 *  entity_keys = {
 *      "id" = "id",
 *      "label" = "question",
 *      "created" = "created",
 *      "changed" = "changed",
 *      "status" = "status",
 *      "published" = "status",
 *  },
 *  links = {
 *      "canonical" = "/faq/{meeg_ninho_faq}",
 *      "add" = "/faq/cadastrar/",
 *      "edit" = "/faq/editar/{meeg_ninho_faq}",
 *      "delete" = "/faq/deletar/{meeg_ninho_faq}",
 *      "collection" = "/faq",
 *  },
 *  field_ui_base_rote = "entity.meeg_ninho_faq.settings",
 *  common_reference_target = TRUE,
 * )
 */
class FAQ extends ContentEntityBase implements FAQInterface
{
    use EntityChangedTrait;
    use EntityPublishedTrait;

    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return $this->get('question')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuestion($question)
    {
        $this->set('question', $question);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAnswer()
    {
        return $this->get('answer')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setAnswer($answer)
    {
        $this->set('answer', $answer);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        return $this->get('product_id')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function setProduct(ProductInterface $product)
    {
        $this->set('product_id', $product->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTheme()
    {
        return $this->get('theme_id')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function setTheme(TermInterface $theme)
    {
        $this->set('theme_id', $theme->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->get('class_id')->entity;
    }

    /**
     * {`@inheritdoc}
     */
    public function setClass(TermInterface $class)
    {
        $this->set('class_id', $class->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);
        $fields += static::publishedBaseFieldDefinitions($entity_type);

        $fields['question'] = BaseFieldDefinition::create('string_long')
            ->setLabel('Pergunta')
            ->setDescription('A pergunta do FAQ.')
            ->setRequired(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'visible',
                'type' => 'basic_string',
                'weight' => 1,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textarea',
                'settings' => ['rows' => 3],
                'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['answer'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Resposta')
            ->setDescription('A resposta para a pergunta do FAQ.')
            ->setRequired(TRUE)
            ->setSettings([
                'default_value' => '',
                'text_processing' => 0
            ])
            ->setDisplayOptions('view', [
                'label' => 'visible',
                'type' => 'text_textarea_with_summary',
                'weight' => 2,
            ])
            ->setDisplayOptions('form', [
                'type' => 'text_textarea_with_summary',
                'settings' => ['rows' => 4],
                'weight' => 2,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['product_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel('Produto')
            ->setDescription('O produto vinculado com esta pergunta.')
            ->setRequired(FALSE)
            ->setSettings([
                'target_type' => 'meeg_ninho_product',
                'handler' => 'default',
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'product',
                'weight' => 4,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'placeholder' => '',
                ],
                'weight' => 4,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['class_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel('Categoria')
            ->setDescription('A categoria vinculada com esta pergunta.')
            ->setRequired(FALSE)
            ->setSettings([
                'target_type' => 'taxonomy_term',
                'handler' => 'default:taxonomy_term',
                'handler_settings' => [
                    'target_bundles' => [
                        'class' => 'class',
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 5,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
                'weight' => 5,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['theme_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel('Tema')
            ->setDescription('O tema vinculado com esta pergunta.')
            ->setRequired(FALSE)
            ->setSettings([
                'target_type' => 'taxonomy_term',
                'handler' => 'default:taxonomy_term',
                'handler_settings' => [
                    'target_bundles' => [
                        'theme' => 'theme',
                    ],
                ],
            ])
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'entity_reference_label',
                'weight' => 3,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => 60,
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
                'weight' => 3,
            ])
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayConfigurable('form', TRUE);

        $fields['status']
            ->setLabel('Ativada')
            ->setDescription('Define se a pergunta está ativada ou não para mostrar ao público.')
            ->setDisplayOptions('form', [
                'type' => 'boolean_checkbox',
                'settings' => [
                    'display_label' => TRUE,
                ],
                'weight' => 6,
            ])
            ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel('Criada')
            ->setDescription('A data e hora de quando a pergunta foi criada.')
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
      
        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel('Alterada')
            ->setDescription('A data e hora mais recente de quando a pergunta foi editada.');

        return $fields;
    }
}