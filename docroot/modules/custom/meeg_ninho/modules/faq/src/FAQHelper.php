<?php

namespace Drupal\meeg_ninho_faq;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Url;

class FAQHelper
{
    /**
     * Entity FAQ.
     * 
     * @var Drupal\meeg_ninho_faq\Entity\FAQ
     */
    protected $faq;

    /**
     * Entity Product.
     * 
     * @var Drupal\meeg_ninho_product\Entity\Product
     */
    protected $product;

    /**
     * Entity Term.
     * 
     * @var Drupal\taxonomy\Entity\Term
     */
    protected $term;

    /**
     * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
     */
    public function __construct(EntityTypeManager $entity_type_manager)
    {
        $this->faq = $entity_type_manager->getStorage('meeg_ninho_faq');
        $this->product = $entity_type_manager->getStorage('meeg_ninho_product');
        $this->term = $entity_type_manager->getStorage('taxonomy_term');
    }

    /**
     * Get the FAQ questions sorted by the tags (class, product).
     * 
     * @param string $check_path
     *  (optional)
     *  The string path to get only the FAQ linked to the class or product tags with the same path.
     *  If null, then it will get all FAQ questions.
     * 
     * @param string $ascending
     *  (optional)
     *  Sort alphabetically by ascending order if true, or descending order if false.
     * 
     * @return array
     *  Array of entity objects.
     */
    public function getFAQSortedByTag($check_path = null, $ascending = true)
    {
        $faq = $this->faq->loadMultiple(
            $this->faq->getQuery()
                ->condition('status', true)
                ->execute()
        );

        if (empty($faq)) {
            return [];
        }

        $data = [];
        foreach ($faq as $item) {
            if (!empty($check_path)) {
                $path = !empty($item->getProduct()) ? $item->getProduct()->toUrl()->toString() : $item->getClass()->toUrl()->toString();
                if (strcmp($check_path, $path) === 0) {
                    $key = !empty($item->getProduct()) ? $item->getProduct()->getTitle() : $item->getClass()->getName();
                    $item->tag = $key;
                    $data[$key] = $item;
                }
            }
            else {
                $key = !empty($item->getProduct()) ? $item->getProduct()->getTitle() : $item->getClass()->getName();
                $item->tag = $key;
                $data[$key] = $item;
            }
        }

        if ($ascending === false) {
            krsort($data);
        }
        else {
            ksort($data);
        }

        return $data;
    }

    /**
     * Get the FAQ selector ordered alphabetically.
     * 
     * @param string $check_path
     *  (optional)
     *  The string path to mark a class or product as selected if it has the same path.
     *  If null then it will not check.
     * 
     * @return array
     *  Array of entity objects.
     */
    public function getFAQSelector($check_path = null)
    {
        $data = [];

        $classes = $this->term->loadMultiple(
            $this->term->getQuery()
                ->condition('status', true)
                ->condition('vid', 'class')
                ->sort('name')
                ->execute()
        );

        if (!empty($classes)) {
            foreach($classes as $class) {
                $products = $this->product->loadMultiple(
                    $this->product->getQuery()
                        ->condition('status', true)
                        ->condition('class_id', $class->id())
                        ->sort('title')
                        ->execute()
                );

                $class->slug = $this->hasQuestions($class->id()) === true ? $class->toUrl()->toString() : null;

                $data[] = [
                    'class' => $class,
                    'products' => $products,
                ];
            }
        }

        if (!empty($data) && !empty($check_path)) {
            foreach ($data as $key => $item) {
                if ($this->entityPathIsEqual($item['class'], $check_path)) {
                    $data[$key]['class']->selected = true;
                    break;
                }
                else {
                    foreach ($item['products'] as $key_prod => $product) {
                        if ($this->entityPathIsEqual($product, $check_path)) {
                            $data[$key]['products'][$key_prod]->selected = true;
                            break 2;
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Check if there are questions for the class or product FAQ.
     * 
     * @param integer $value
     *  The value for the condition on the query. Usually it must be an ID.
     * 
     * @param string $field
     *  (optional)
     *  The field for the condition on the query. It must be one of the options:
     *   - class_id (Default)
     *   - product_id
     * 
     * @return boolean
     */
    public function hasQuestions($value, $field = 'class_id')
    {
        $count = $this->faq->getQuery()
            ->condition('status', true)
            ->condition($field, $value)
            ->count()
            ->execute();

        if ($count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Check and get the path from the FAQ.
     * 
     * @param string $url_route
     *  (optional)
     *  The url to check the its path. If null then it will use the '<current>' route.
     * 
     * @return string|null
     *  Returns null if the path is '/faq', or the path of the child page (ex: /faq/ninho).
     */
    public function checkFAQPath($url_route = '<current>')
    {
        $path = Url::fromRoute($url_route)->toString();
        if (strcmp('/faq', $path) === 0) {
            return null;
        }
        return str_replace('/faq', '', $path);
    }

    /**
     * Check if a given entity url path is equal to a given path.
     * 
     * @param $entity
     *  The entity object to check.
     * 
     * @param string $path
     *  The string path to check.
     * 
     * @return boolean
     */
    private function entityPathIsEqual($entity, string $path)
    {
        if (strcmp($path, $entity->toUrl()->toString()) === 0) {
            return true;
        }
        return false;
    }
}