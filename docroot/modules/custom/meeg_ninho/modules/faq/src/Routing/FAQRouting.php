<?php

namespace Drupal\meeg_ninho_faq\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic routes for FAQ.
 */
class FAQRouting
{
    /**
     * {@inheritDoc}
     */
    public function routes()
    {
        $collection = new RouteCollection();
        $entity = \Drupal::entityTypeManager();
        $controller = '\Drupal\meeg_ninho_faq\Controller\FAQController';

        $classes = $entity->getStorage('taxonomy_term')->loadByProperties([
            'vid' => 'class'
        ]);

        if (!empty($classes)) {
            foreach ($classes as $class) {
                $path = $class->toUrl()->toString();

                if (strpos($path, '/taxonomy/term/') === false) {
                    $path = substr($path, 1);
                    $route = new Route(
                        "faq/{$path}",
                        [
                            '_controller' => "{$controller}::list",
                            '_title' => 'FAQ',
                        ],
                        [
                            '_permission' => 'access content',
                        ]
                    );
                    $route_id = "meeg_ninho_faq.faq." . str_replace('-', '_', $path);
                    $collection->add($route_id, $route);
                }
            }
        }

        $products = $entity->getStorage('meeg_ninho_product')->loadMultiple(
            $entity->getStorage('meeg_ninho_product')->getQuery()->execute()
        );

        if (!empty($products)) {
            foreach ($products as $product) {
                $path = $product->toUrl()->toString();

                if (strpos($path, '/produto/') === false) {
                    $path = substr($path, 1);
                    $route = new Route(
                        "faq/{$path}",
                        [
                            '_controller' => "{$controller}::list",
                            '_title' => 'FAQ',
                        ],
                        [
                            '_permission' => 'access content',
                        ]
                    );
                    $route_id = "meeg_ninho_faq.faq." . str_replace('-', '_', $path);
                    $collection->add($route_id, $route);
                }
            }
        }

        return $collection;
    }
}