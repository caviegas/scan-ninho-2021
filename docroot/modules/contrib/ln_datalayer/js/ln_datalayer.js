Date.prototype.toIsoString = function () {
    var timeZoneOffset = -this.getTimezoneOffset(),
        dif = timeZoneOffset >= 0 ? '+' : '-',
        returnDateTime = function (num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + returnDateTime(this.getMonth() + 1) +
        '-' + returnDateTime(this.getDate()) +
        'T' + returnDateTime(this.getHours()) +
        ':' + returnDateTime(this.getMinutes()) +
        ':' + returnDateTime(this.getSeconds()) +
        '.' + returnDateTime(this.getMilliseconds()) +
        dif + returnDateTime(timeZoneOffset / 60) +
        ':' + returnDateTime(timeZoneOffset % 60);
}

if(dataLayer != undefined) {
    dataLayer[0]['hitTimeStamp'] = "" + new Date().toIsoString() + "";
}
