<?php

  namespace Drupal\ln_datalayer\Form;

  use Drupal\Core\Form\ConfigFormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\ln_datalayer\Controller\LnDatalayerController;
  use Symfony\Component\DependencyInjection\ContainerInterface;

  /**
   * Class LightnestDatalayerForm.
   *
   * @package Drupal\ln_datalayer\Form
   */
  class LightnestDatalayerForm extends ConfigFormBase {

    /**
     * Configuration state Drupal Site.
     *
     * @var object
     */

    protected $langManager;

    /**
     * Constructs a LightnestDatalayerForm object.
     *
     * @param object $lang_manager
     *   The language manager.
     */
    public function __construct($lang_manager) {
      $this->langManager = $lang_manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('language_manager')
      );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'ln_datalayer_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
      return ['ln_datalayer.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $datalayer_settings = $this->config('ln_datalayer.settings');

      $form['general'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Data layer output keys'),
        '#description' => $this->t('Define keys used in the datalayer output. Keys for field values are configurable via the field edit form.'),
      ];

      // Build form elements.
      $siteZonesList = LnDatalayerController::getZones();
      $updated_siteZonesList = $this->getTranslatedArrayOptions($siteZonesList);
      $zone = $datalayer_settings->get('zone');
      $form['general']['zone'] = [
        '#type' => 'select',
        '#title' => $this->t('Site Zone'),
        '#description' => $this->t('Country or Site specific zone'),
        '#default_value' => isset($zone) ? $zone : 0,
        '#options' => $updated_siteZonesList,
      ];

      $businessesList = LnDatalayerController::getBusinesses();
      $updated_businessesList = $this->getTranslatedArrayOptions($businessesList);
      $business = $datalayer_settings->get('business');
      $form['general']['business'] = [
        '#type' => 'select',
        '#title' => $this->t('Business'),
        '#description' => $this->t('Business for site brand (e.g., Nescafe)'),
        '#default_value' => isset($business) ? $business : 0,
        '#options' => $updated_businessesList,
      ];

      $advertiser_id = $datalayer_settings->get('advertiser_id');
      $form['general']['advertiser_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parent Advertiser ID'),
        '#description' => $this->t('Advertiser ID for Floodlight added on the site'),
        '#default_value' => isset($advertiser_id) ? $advertiser_id : '',
        '#size' => 30,
      ];

      $countriesList = LnDatalayerController::getcountries();
      $updated_countriesList = $this->getTranslatedArrayOptions($countriesList);
      $country = $datalayer_settings->get('country');
      $form['general']['country'] = [
        '#type' => 'select',
        '#title' => $this->t('Country'),
        '#description' => $this->t('Country of website'),
        '#default_value' => isset($country) ? $country : 0,
        '#options' => $updated_countriesList,
      ];

      $business_unit = $datalayer_settings->get('business_unit');
      $form['general']['business_unit'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Business Unit'),
        '#description' => $this->t('Business unit of the site (e.g., Health, Food)'),
        '#default_value' => isset($business_unit) ? $business_unit : 'Business Unit',
        '#size' => 30,
      ];

      $count_group_tag_string = $datalayer_settings->get('count_group_tag_string');
      $form['general']['count_group_tag_string'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Floodlight Count Group Tag String'),
        '#description' => $this->t('The Group Tag String for the site’s Floodlight Count'),
        '#default_value' => isset($count_group_tag_string) ? $count_group_tag_string : '',
        '#size' => 30,
      ];

      $languagesList = LnDatalayerController::getlanguages();
      $updated_languagesList = $this->getTranslatedArrayOptions($languagesList);
      $language = $datalayer_settings->get('language');
      $form['general']['language'] = [
        '#type' => 'select',
        '#title' => $this->t('Language'),
        '#description' => $this->t('Language of the website (Select \'Multilanguage\' if site has more than one language)'),
        '#default_value' => isset($language) ? $language : 0,
        '#options' => $updated_languagesList,
      ];

      $brandsList = LnDatalayerController::getBrands();
      $updated_brandsList = $this->getTranslatedArrayOptions($brandsList);
      $brand = $datalayer_settings->get('brand');
      $form['general']['brand'] = [
        '#type' => 'select',
        '#title' => $this->t('Brand'),
        '#description' => $this->t('Contains the Brand featured on the site, based on Nestlé\'s specifications'),
        '#default_value' => isset($brand) ? $brand : 0,
        '#options' => $updated_brandsList,
      ];

      $sales_group_tag_string = $datalayer_settings->get('sales_group_tag_string');
      $form['general']['sales_group_tag_string'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Floodlight Sales Group Tag String'),
        '#description' => $this->t('The Group Tag String for the site’s Floodlight Sales'),
        '#default_value' => isset($sales_group_tag_string) ? $sales_group_tag_string : '',
        '#size' => 30,
      ];

      $sitePropertiesList = LnDatalayerController::getProperties();
      $updated_sitePropertiesList = $this->getTranslatedArrayOptions($sitePropertiesList);
      $properties = $datalayer_settings->get('properties');
      $form['general']['properties'] = [
        '#type' => 'select',
        '#title' => $this->t('Site Properties'),
        '#description' => $this->t('Additional properties for site required in GTM'),
        '#options' => $updated_sitePropertiesList,
        '#default_value' => isset($properties) ? $properties : '0',
      ];

      $subBrandsList = LnDatalayerController::getSubBrands();
      $updated_subBrandsList = $this->getTranslatedArrayOptions($subBrandsList);
      $sub_brand = $datalayer_settings->get('sub_brand');
      $form['general']['sub_brand'] = [
        '#type' => 'select',
        '#title' => $this->t('Sub Brand'),
        '#description' => $this->t('Contains the SubBrand featured on the site, based on Nestlé\'s specifications'),
        '#default_value' => isset($sub_brand) ? $sub_brand : 0,
        '#options' => $updated_subBrandsList,
      ];

      $video_type = $datalayer_settings->get('video_type');
      $form['general']['video_type'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Video Type'),
        '#description' => $this->t('Video type of the website (e.g., Promo, Explainer, Recipe, etc.)'),
        '#default_value' => isset($video_type) ? $video_type : '',
        '#size' => 30,
      ];

      $propertyTyepesList = LnDatalayerController::getProperteyTypes();
      $updated_propertyTyepesList = $this->getTranslatedArrayOptions($propertyTyepesList);
      $siteType = $datalayer_settings->get('siteType');
      $form['general']['siteType'] = [
        '#type' => 'select',
        '#title' => $this->t('Property Type'),
        '#description' => $this->t('Type of site for user (e.g., portal, website)'),
        '#default_value' => isset($siteType) ? $siteType : 'Site',
        '#options' => $updated_propertyTyepesList,
      ];

      $propertyStatusList = LnDatalayerController::getPropertyStatus();
      $updated_propertyStatusList = $this->getTranslatedArrayOptions($propertyStatusList);
      $property_status = $datalayer_settings->get('property_status');
      $form['general']['property_status'] = [
        '#type' => 'select',
        '#title' => $this->t('Property Status'),
        '#description' => $this->t('Status of the site (e.g., production, staging, live)'),
        '#default_value' => isset($property_status) ? $property_status : 'Property Status',
        '#options' => $updated_propertyStatusList,
      ];

      $sign_up = $datalayer_settings->get('sign_up');
      $form['general']['sign_up'] = [
        '#type' => 'radios',
        '#title' => $this->t('Sign Up'),
        '#description' => $this->t('If the site has sign_up functionality'),
        '#default_value' => isset($sign_up) ? $sign_up : 0,
        '#options' => ['1' => $this->t('True'), '0' => $this->t('False')],
      ];

      $technologiesList = LnDatalayerController::getTechnologies();
      $updated_technologiesList = $this->getTranslatedArrayOptions($technologiesList);
      $technology = $datalayer_settings->get('technology');
      $form['general']['technology'] = [
        '#type' => 'select',
        '#title' => $this->t('CMS'),
        '#description' => $this->t('Technology of site build (e.g., Drupal)'),
        '#default_value' => isset($technology) ? $technology : 'Drupal',
        '#options' => $updated_technologiesList,
      ];

      $digi_pi_id = $datalayer_settings->get('digi_pi_id');
      $form['general']['digi_pi_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('DigiPiID'),
        '#description' => $this->t('DigiPi ID value assigned to the website'),
        '#default_value' => isset($digi_pi_id) ? $digi_pi_id : '####',
        '#size' => 30,
      ];

      $login = $datalayer_settings->get('login');
      $form['general']['login'] = [
        '#type' => 'radios',
        '#title' => $this->t('Login'),
        '#description' => $this->t('If the site has login functionality'),
        '#default_value' => isset($login) ? $login : 0,
        '#options' => ['1' => $this->t('True'), '0' => $this->t('False')],
      ];

      $targetAudiencesList = LnDatalayerController::getTargetAudiences();
      $updated_targetAudiencesList = $this->getTranslatedArrayOptions($targetAudiencesList);
      $target_audience = $datalayer_settings->get('target_audience');
      $form['general']['target_audience'] = [
        '#type' => 'select',
        '#title' => $this->t('Target Audience'),
        '#description' => $this->t('Target audience of the website/app (e.g., B2B, B2C, B2E)'),
        '#default_value' => isset($target_audience) ? $target_audience : 'B2C',
        '#options' => $updated_targetAudiencesList,
      ];

      $go_live_date = $datalayer_settings->get('go_live_date');
      $form['general']['go_live_date'] = [
        '#type' => 'date',
        '#format' => 'm/d/Y',
        '#title' => $this->t('Golive Date'),
        '#description' => $this->t('Date when site went live'),
        '#default_value' => isset($go_live_date) ? $go_live_date : 'Golive Date',
        '#size' => 30,
      ];

      $ecommerce = $datalayer_settings->get('ecommerce');
      $form['general']['ecommerce'] = [
        '#type' => 'radios',
        '#title' => $this->t('Ecommerce'),
        '#description' => $this->t('If the site has ecommerce functionality'),
        '#default_value' => isset($ecommerce) ? $ecommerce : 0,
        '#options' => ['1' => $this->t('True'), '0' => $this->t('False')],
      ];

      $site_category = $datalayer_settings->get('site_category');
      $form['general']['site_category'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Site Category'),
        '#description' => $this->t('Category assigned to a page (Values vary from site to site - e.g., Food, Fitness)'),
        '#default_value' => isset($site_category) ? $site_category : 'Site Category',
        '#size' => 30,
      ];

      $conversion_page_type = $datalayer_settings->get('conversion_page_type');
      $form['general']['conversion_page_type'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Conversion Page Type'),
        '#description' => $this->t('Type of page where the user is completing 
        a transaction or conversion (Values can vary from site to site)'),
        '#default_value' => isset($conversion_page_type) ? $conversion_page_type : 'Conversion Page Type',
        '#size' => 30,
      ];

      $coupon_print = $datalayer_settings->get('coupon_print');
      $form['general']['coupon_print'] = [
        '#type' => 'radios',
        '#title' => $this->t('Coupon Print'),
        '#description' => $this->t('If the site has coupon_print functionality'),
        '#default_value' => isset($coupon_print) ? $coupon_print : 0,
        '#options' => ['1' => $this->t('True'), '0' => $this->t('False')],
      ];

      $page_subsection = $datalayer_settings->get('page_subsection');
      $form['general']['page_subsection'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Page Subsection'),
        '#description' => $this->t('Subsection of the page (Values vary from site to site - e.g., Petcare, Beverages)'),
        '#default_value' => isset($page_subsection) ? $page_subsection : 'Page Subsection',
        '#size' => 30,
      ];

      $languages = $this->langManager->getLanguages();
      $language_count = count($languages);
      if($language_count == 1) {
        foreach ($languages as $lang => $data) {
          $form['general']['website_url_' . $lang] = [
            '#type' => 'textfield',
            '#title' => $data->getName() . ': Website URL',
            '#description' => $this->t('Website URL for the specific language'),
            '#default_value' => $datalayer_settings->get('website_url_' . $lang),
          ];
        }
      } else {
        $form['general']['empty_markup'] = [
          '#type' => 'markup',
          '#markup' => '',
          '#size' => 30,
          '#prefix' => '<div class = "form-item">',
          '#suffix' => '</div>'
        ];
      }

      $checkout = $datalayer_settings->get('checkout');
      $form['general']['checkout'] = [
        '#type' => 'radios',
        '#title' => $this->t('Checkout'),
        '#description' => $this->t('If the site has Checkout functionality'),
        '#default_value' => isset($checkout) ? $checkout : 0,
        '#options' => ['1' => $this->t('True'), '0' => $this->t('False')],
      ];

      if($language_count > 1) {
        foreach ($languages as $lang => $data) {
          $form['general']['website_url_' . $lang] = [
            '#type' => 'textfield',
            '#title' => $data->getName() . ': Website URL',
            '#description' => $this->t('Website URL for the specific language'),
            '#default_value' => $datalayer_settings->get('website_url_' . $lang),
          ];
        }
      }

      $form['#attached']['library'][] = 'ln_datalayer/ln_datalayer.style';

      return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      // Save default form settings.
      $configSettings = $this->configFactory->getEditable('ln_datalayer.settings');
      $configSettings->set('brand', $form_state->getValue('brand'))
        ->set('sub_brand', $form_state->getValue('sub_brand'))
        ->set('zone', $form_state->getValue('zone'))
        ->set('country', $form_state->getValue('country'))
        ->set('language', $form_state->getValue('language'))
        ->set('business', $form_state->getValue('business'))
        ->set('siteType', $form_state->getValue('siteType'))
        ->set('technology', $form_state->getValue('technology'))
        ->set('properties', $form_state->getValue('properties'))
        ->set('digi_pi_id', $form_state->getValue('digi_pi_id'))
        ->set('business_unit', $form_state->getValue('business_unit'))
        ->set('conversion_page_type', $form_state->getValue('conversion_page_type'))
        ->set('property_status', $form_state->getValue('property_status'))
        ->set('go_live_date', $form_state->getValue('go_live_date'))
        ->set('target_audience', $form_state->getValue('target_audience'))
        ->set('site_category', $form_state->getValue('site_category'))
        ->set('page_subsection', $form_state->getValue('page_subsection'))
        ->set('advertiser_id', $form_state->getValue('advertiser_id'))
        ->set('ecommerce', $form_state->getValue('ecommerce'))
        ->set('login', $form_state->getValue('login'))
        ->set('sign_up', $form_state->getValue('sign_up'))
        ->set('coupon_print', $form_state->getValue('coupon_print'))
        ->set('checkout', $form_state->getValue('checkout'))
        ->set('video_type', $form_state->getValue('video_type'))
        ->set('count_group_tag_string', $form_state->getValue('count_group_tag_string'))
        ->set('sales_group_tag_string', $form_state->getValue('sales_group_tag_string'));

      $languages = $this->langManager->getLanguages();
      foreach ($languages as $lang => $data) {
        $configSettings->set('website_url_' . $lang, $form_state->getValue('website_url_' . $lang));
      }

      $configSettings->save();
      parent::submitForm($form, $form_state);
    }

    /**
     * Add translation on the option values.
     *
     * @param array $array_list
     *   Option array.
     *
     * @return array
     *   Return array.
     */
    public function getTranslatedArrayOptions(array $array_list) {
      $updatedList = [];
      foreach ($array_list as $code => $name) {
        $updatedList[$code] = $this->t($name);
      }
      return $updatedList;
    }

  }